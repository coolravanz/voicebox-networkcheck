# webrtc-getstatus
This application will test the network statistics and ablity of your browser or network  to connect a websocket
#how to install
step 1 : should install nodejs
```bash
sudo apt-get install nodejs
sudo apt-get install nodejs-legacy
sudo apt-get install npm
```
step 2 : clone the source code from this repository
```bash
git clone https://github.com/enfingit/webrtc-getstatus.git
```
step 2 : install express and socketio
```bash
cd webrtc-getstatus
sudo npm install express
sudo npm install socket.io
```

step 3 : run the application
```bash
sudo node server.js
```

Now console will show the message  Server Running At 8080 . Your application is ready
```bash
Please load the url  :   https://[your domain]:8080
```
You can change the turnserver on js/main.js in static directory
```bash
var servers = {
  'iceServers': [
    {
      'url': 'turn:domain.com:3478',
      'credential': 'user',
      'username': 'password'
    }
  ]
```
You can change the port by editing 
```bash
server.js in root directory
```

This application uses self-signed certificate so you need to allow insecure domain
for using original ssl certificate put your certificate files in 
```bash
  cert directory in root
```

