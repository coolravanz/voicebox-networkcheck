// Load required modules
var port     =  8080;
var https    = require("https");              // http server core module
var express  = require("express");           // web framework external module
var io       = require("socket.io");         // web socket external module

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var httpApp = express();
httpApp.use(express.static(__dirname + "/static/"));

var fs = require('fs');
var options = {
    key: fs.readFileSync('cert/server.key'),
    cert: fs.readFileSync('cert/server.crt')
};
// Start Express http server on port 90
var webServer = https.createServer(options, httpApp).listen(port);

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {"log level":1});
console.log('Server Running At '+port);

